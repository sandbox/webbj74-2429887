<?php

/**
 * @file
 * Contains TroublemakerErrorA.
 */

/**
 * Class TroublemakerSegfaultA is used to generate a PHP Error
 */
class TroublemakerErrorA {
  /**
   * Call this method statically, I dare you!
   */
  public function doNotCallThisStatically()
  {
    return null;
  }
}
