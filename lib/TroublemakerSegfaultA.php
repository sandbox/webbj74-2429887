<?php

/**
 * @file
 * Contains TroublemakerSegfaultA.
 */

/**
 * Class TroublemakerSegfaultA is used to exploit a PHP bug.
 */
class TroublemakerSegfaultA {
  /**
   * Magic property getter
   * @param $x
   */
  public function __get($x) {
    global $_troublemaker_amplifier;
    $_troublemaker_amplifier->incrementOfSorrow++;
  }
}
