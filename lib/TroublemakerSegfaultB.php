<?php

/**
 * @file
 * Contains TroublemakerSegfaultB.
 */

/**
 * Class TroublemakerSegfaultB is used to exploit a PHP bug.
 */
class TroublemakerSegfaultB {
  /**
   * Magic property getter
   * @param $x
   */
  public function __get($x) {
    global $_troublemaker_pain;
    $_troublemaker_pain->incrementOfConfusion++;
  }
}
